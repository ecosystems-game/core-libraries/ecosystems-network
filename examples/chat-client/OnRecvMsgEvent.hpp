#ifndef ECOSYSTEMS_ONRECVMSGEVENT_HPP
#define ECOSYSTEMS_ONRECVMSGEVENT_HPP

#include <iostream>
#include <vector>
#include <string>

#include "MessageArchive.hpp"

#include <eco-network/events/NetworkEvent.hpp>
#include <eco-network/Packet.hpp>
#include <eco-network/Session.hpp>


namespace ecosystems::client {
    class OnRecvMsgEvent : public network::events::NetworkEvent {
    public:
        OnRecvMsgEvent(MessageArchive& message_archive) : network::events::NetworkEvent(), m_message_archive(message_archive) {}

        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<network::events::NetworkObserver> p_observer) override {
            auto username = p_packet.ReadNextArgument<std::string>();
            auto message = p_packet.ReadNextArgument<std::string>();

            m_message_archive.append_message("[" + username + "]: " + message);
        }
    private:
        MessageArchive& m_message_archive;
    };
}

#endif // ECOSYSTEMS_ONRECVMSGEVENT_HPP
