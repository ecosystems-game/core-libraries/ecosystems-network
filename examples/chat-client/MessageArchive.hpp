#ifndef ECOSYSTEMS_MESSAGE_ARCHIVE_HPP
#define ECOSYSTEMS_MESSAGE_ARCHIVE_HPP

#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <thread>

#include <eco-network/events/NetworkEvent.hpp>
#include <eco-network/Packet.hpp>
#include <eco-network/Session.hpp>


namespace ecosystems::client {
    class MessageArchive {
    public:
        MessageArchive() {}

        std::vector<std::string>& get_messages() {
            std::unique_lock<std::mutex> lock(m_message_mutex);
            return m_messages;
        }

        void append_message(std::string& message) {
            std::unique_lock<std::mutex> lock(m_message_mutex);
            m_messages.push_back(message);
        }
    private:
        std::mutex m_message_mutex;
        std::vector<std::string> m_messages;
    };
}

#endif // ECOSYSTEMS_MESSAGE_ARCHIVE_HPP
