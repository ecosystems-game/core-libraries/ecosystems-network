#include <iostream>

#include <eco-network/Client.hpp>

#include "OnRecvMsgEvent.hpp"
#include "MessageArchive.hpp"

#ifdef _WIN32
	void clear() {
		COORD topLeft = { 0, 0 };
		HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO screen;
		DWORD written;

		GetConsoleScreenBufferInfo(console, &screen);
		FillConsoleOutputCharacterA(
			console, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written
		);
		FillConsoleOutputAttribute(
			console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE,
			screen.dwSize.X * screen.dwSize.Y, topLeft, &written
		);
		SetConsoleCursorPosition(console, topLeft);
	}
#else
	void clear() {
		// CSI[2J clears screen, CSI[H moves the cursor to top-left corner
		std::cout << "\x1B[2J\x1B[H";
	}
#endif

int main() {
	std::string username;
	std::cout << "Please enter a username: ";
	std::getline(std::cin, username);
	std::cout << "Connecting you to the chat, " << username.c_str() << "..." << std::endl;
	
	ecosystems::client::MessageArchive message_archive;
	
	auto client = std::make_shared<ecosystems::network::Client>();
	
	client->get_observer()->SubscribeEvent("chat-message", new ecosystems::client::OnRecvMsgEvent(message_archive));

	std::cout << "Attempting to connect to server at 127.0.0.1:2000" << std::endl;
	

	try {
		client->Connect("127.0.0.1", 2000);
		while (!client->get_session()->is_connected());
	}
	catch(...) {
		std::cerr << "Failed to connect to server at 127.0.0.1:2000! Is it running?" << std::endl;
		return -1;
	}

	std::cout << "Client successfully connected to server on 127.0.0.1:2000!" << std::endl;
	
	while (true) {
		clear();
		for (std::string message : message_archive.get_messages()) {
			std::cout << message << std::endl;
		}

		std::string message;
		std::cout << "Message (blank to update): ";
		std::getline(std::cin, message);

		if (message.empty()) {
			continue;
		}

		ecosystems::network::Packet packet(client->get_observer()->get_hook_index("chat-message"));
		packet.AddArgument(username);
		packet.AddArgument(message);

		client->TCP_Send(packet);
	}

	return 0;
}