#include <iostream>
#include <eco-network/Server.hpp>
#include <eco-network/Session.hpp>

#include "OnRecvMsgEvent.hpp"

int main() {
	std::cout << "Starting Server on Port 2000!" << std::endl;
	ecosystems::network::Server server(2000);

	server.get_observer()->SubscribeEvent("chat-message", new ecosystems::server::OnRecvMsgEvent(server));
	
	server.Run();
	std::cout << "Server successfully started on port 2000!";
	while (true);
	
	
	return 0;
}