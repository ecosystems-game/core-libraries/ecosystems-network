#ifndef ECOSYSTEMS_ONRECVMSGEVENT_HPP
#define ECOSYSTEMS_ONRECVMSGEVENT_HPP

#include <iostream>
#include <string>

#include <eco-network/events/NetworkEvent.hpp>
#include <eco-network/Packet.hpp>
#include <eco-network/Session.hpp>
#include <eco-network/Server.hpp>


namespace ecosystems::server {
    class OnRecvMsgEvent : public network::events::NetworkEvent {
    public:
        OnRecvMsgEvent(ecosystems::network::Server& server) : network::events::NetworkEvent(), m_server(server) {
        
        }

        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<network::events::NetworkObserver> p_observer) override {
            auto username = p_packet.ReadNextArgument<std::string>();
            auto message = p_packet.ReadNextArgument<std::string>();

            std::cout << username.c_str() << ": " << message.c_str() << std::endl;

            ecosystems::network::Packet packet(p_observer->get_hook_index("chat-message"));
            packet.AddArgument(username);
            packet.AddArgument(message);

            for (const auto& session_entry : m_server.get_sessions()) {
                const auto& session = session_entry.second;
                session->TCP_Send(packet);
            }
        }
    private:
        ecosystems::network::Server& m_server;
    };
}

#endif // ECOSYSTEMS_ONRECVMSGEVENT_HPP
