#include "EcoApiModule.h"

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>

#include "eco-network/Session.hpp"
#include "eco-network/Server.hpp"
#include "eco-network/exceptions/NetworkException.hpp"
#include "eco-network/events/NetworkObserver.hpp"
#include "eco-network/eco-events/server/EcoSessionConnectEvent.hpp"
#include "eco-network/eco-events/server/EcoSessionTestConnection.hpp"

using namespace ecosystems::network;
using namespace ecosystems::network::exceptions;
using namespace boost::asio::ip;


ECO_API Server::Server(unsigned short p_port)
    : m_port(p_port), m_io_context(), m_observer(new events::NetworkObserver()),
      m_udp_socket(std::make_unique<udp::socket>(m_io_context, udp::endpoint(udp::v6(), p_port))),
      m_tcp_acceptor(m_io_context, tcp::endpoint(tcp::v6(), p_port))
{
    // Bind the default network events used by the internal library (eco_events)
    // The client will have a matching list of these hook definitions
    m_observer->SubscribeEvent("eco-session-connect", new events::eco_events::server::EcoSessionConnectEvent(this));
    m_observer->SubscribeEvent("eco-session-test", new events::server::eco_events::EcoSessionTestConnection(this));

}

void Server::Run() {
    std::cout << "[Network - Server] Starting Ecosystems Session Server..." << std::endl;
    m_is_running = true;
    m_ping_thread = std::thread(&Server::TestActiveSessions, this);
    m_server_run_thread = std::thread(&Server::RunThread, this);
}

void Server::RunThread() {
    TCP_BeginAccept();
    std::cout << "[Network - Server/TCP] TCP Acceptor is now bound to listen for incoming connections on Port: " << m_port << std::endl;
    UDP_BeginReceive();
    std::cout << "[Network - Server/UDP] UDP Socket is now bound to listen for incoming datagrams on Port " << m_port << std::endl;
    m_io_context.run();
}

void Server::Shutdown() {
    std::cout << "[Network/Server] The server is shutting down..." << std::endl;
    m_is_running = false;
    m_server_run_thread.join();
    m_ping_thread.join();
    std::cout << "[Network/Server] The server has been gracefully close." << std::endl;
}

std::shared_ptr<Session> Server::get_session(boost::uuids::uuid p_uuid) {

    auto it = m_sessions.find(p_uuid);
    if(it != m_sessions.end()) {
        return it->second;
    }

    return nullptr;
}

void Server::TCP_BeginAccept() {
    auto session = std::make_shared<Session>(m_io_context, m_observer, m_udp_socket);
    m_tcp_acceptor.async_accept(session->get_tcp_socket(),
                                boost::bind(&Server::TCP_HandleAccept, this, session,
                                            boost::asio::placeholders::error));
}

void Server::TCP_HandleAccept(std::shared_ptr<Session> p_session, const boost::system::error_code &p_error) {
    if(!p_error) {
        std::string ip_address = p_session->get_tcp_socket().remote_endpoint().address().to_string();
        std::string port = std::to_string(p_session->get_tcp_socket().remote_endpoint().port());
        std::string ep_string = ip_address + ":" + port;
        p_session->set_session_uuid(boost::uuids::random_generator()());
        p_session->set_tcp_connected(true);

        // When we accept TCP we update both the UDP and TCP last received time, because the time the session was created,
        // could be any number of seconds before HandleAccept is called.
        p_session->update_tcp_last_receved();
        p_session->update_udp_last_received();
#ifdef NETWORK_VERBOSE
        std::cout << "[Network - Server/TCP] Accepted a new incoming TCP connection on " << ep_string.c_str() << std::endl;
        std:: cout << "[Network - Server/TCP] A new session has been established, TCP has been linked to session " << p_session->get_session_uuid() << std::endl;
#endif

        m_sessions.insert({p_session->get_session_uuid(), p_session});
        p_session->TCP_BeginReceive();

        std::string uuid = boost::uuids::to_string(p_session->get_session_uuid());
        Packet session_packet(m_observer->get_hook_index("eco-session-connect"));
        session_packet.AddArgument(false);
        session_packet.AddArgument(uuid);
        p_session->TCP_Send(session_packet);
#ifdef NETWORK_VERBOSE
        std::cout << "[Network Server/Reg] Received Eco-Session Connection Request, Sending UUID to Client: " << uuid << std::endl;
#endif
        TCP_BeginAccept();
    } else {
        // TODO: Implement Network Error Handling
        std::cerr << "[Network - Server/TCP] There was an error accepting an incoming TCP connection. " << p_error.message() << std::endl;
    }
}

void Server::UDP_BeginReceive() {
    m_udp_socket->async_receive_from(
            boost::asio::buffer(*m_recv_buffer), m_remote_ep,
            boost::bind(&Server::UDP_HandleReceive, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
}

// Note: Our UDP Receive method will just attempt to interpret packets as they come in. It will not attempt to join up packets by chunks.
void Server::UDP_HandleReceive(const boost::system::error_code p_error, size_t p_bytes_read) {
    if(!p_error) {
        Packet packet(&(*m_recv_buffer)[0], p_bytes_read, UDP);

        // Only Accept Packets whose id's are in our observer
        if(!m_observer->is_hook_index_in_map(packet.get_event_hook_index())) {
            return;
        }

        std::lock_guard<std::mutex> session_lock(m_sessions_mutex);

        auto it = m_udp_session_keys.find(m_remote_ep);
        if(it != m_udp_session_keys.end()){
            try {
                auto session = m_sessions[it->second];
                session->update_udp_last_received();
                m_observer->Trigger(packet.get_event_hook_index(), packet, session);
            } catch (NetworkException& e) {
                std::cerr << e.what() << std::endl;
            } catch( const boost::system::system_error& ex )
            {
                std::cerr << "An error was thrown trying to get information from the socket. The remote connection may have been closed." << std::endl;
            }
        } else {
#ifdef NETWORK_VERBOSE
            std::cout << "[Network - Server/UDP] Datagram from unknown connection received. Creating temporary session for binding packets. " << std::endl;
            std::string remote_ep_str = m_remote_ep.address().to_string() + ":" + std::to_string(m_remote_ep.port());
            std::cout << "[Network - Server/UDP] Remote Endpoint: " << remote_ep_str << std::endl;
#endif
            // We don't accept packets from any session that hasn't been fully connected to a TCP Session except for eco-session-connection packets
            if(packet.get_event_hook_index() != get_observer()->get_hook_index("eco-session-connect")) {
                return;
            }

            // If we cannot find the endpoint in our session_key map,
            // Create a temporary session that only has a UDP endpont,
            // leaving the is_tcp_connected flag to false.
            auto session = std::make_shared<Session>(m_io_context, m_observer, m_udp_socket);
            session->set_udp_endpoint(m_remote_ep);
            session->set_tcp_connected(false);
            try {
                m_observer->Trigger(packet.get_event_hook_index(), packet, session);
            } catch (NetworkException& e) {
                std::cerr << e.what() << std::endl;
            } catch( const boost::system::system_error& ex )
            {
                std::cerr << "An error was thrown trying to get information from the socket. The remote connection may have been closed." << std::endl;
            }
        }
    } else {
#ifdef NETWORK_VERBOSE
        std::cerr << "[Network/UDP] Error receiving data: " << p_error.message() << std::endl;
#endif
    }

    UDP_BeginReceive();
}

void Server::UDP_BindEndpointToSession(boost::asio::ip::udp::endpoint& p_endpoint, boost::uuids::uuid p_uuid) {
    auto it = m_sessions.find(p_uuid);
    auto udp_it = m_udp_session_keys.find(p_endpoint);

    if(udp_it != m_udp_session_keys.end()) {
#ifdef NETWORK_VERBOSE
        std::string ep_str = p_endpoint.address().to_string() + ":" + std::to_string(p_endpoint.port());
                std::cerr << "[Network - Server/UDP] Attempt to bind UDP Endpoint(" << ep_str.c_str() << ") to UUID " << p_uuid << " Failed. The Endpoint was already bound to the Session, " << p_uuid << std::endl;
#endif
        // TODO:  Link to Network Error Handler
        return;
    }

    if(it != m_sessions.end()) {
        m_udp_session_keys[p_endpoint] = p_uuid;
        it->second->set_udp_endpoint(p_endpoint);
        it->second->set_udp_connected(true);
#ifdef NETWORK_VERBOSE
        std::string ep_str = p_endpoint.address().to_string() + ":" + std::to_string(p_endpoint.port());
                std::cout << "[Network - Server/UDP] UDP Endpoint(" << ep_str.c_str() << ") bound to session " << p_uuid << std::endl;
#endif
        std::cout << "[Network] A new EcoSession has been successfully established!\n"
                     "    EcoSession-UUID:" << it->second->get_session_uuid() << "\n"
                                                                                 "    TCP Endpoint:" << it->second->get_tcp_socket().remote_endpoint() << "\n"
                                                                                                                                                          "    UDP Endpoint:" << p_endpoint << "\n";
    } else {
#ifdef NETWORK_VERBOSE
        std::string ep_str = p_endpoint.address().to_string() + ":" + std::to_string(p_endpoint.port());
                std::cerr << "[Network - Server/UDP] Attempted to bind UDP Endpoint(" << ep_str.c_str() << ") to UUID " << p_uuid << " but the TCP session by the given ID was not found." << std::endl;
#endif
    }
}

size_t Server::UDP_SendUntilSignaled(Packet& p_packet, uint32_t p_relay_usec, uint32_t p_timeout_usec, std::shared_ptr<Session>& p_session){
    std::lock_guard<std::mutex> thread_lock(m_signaled_packets_mutex);
    size_t index = m_signaled_packet_index;
    m_signaled_packets.insert({index, SignaledPacket(p_packet, p_relay_usec, p_timeout_usec, p_session)});
    ++m_signaled_packet_index;

    if(!is_signaled_packets_running) {
        if(m_signaled_packets_thread.joinable()) {
            m_signaled_packets_thread.join();
        }
        is_signaled_packets_running = true;
        m_signaled_packets_thread = std::thread(&Server::UDP_SendSignaledPackets, this);
    }

    return index;
}

void Server::UDP_SignalStopSend(size_t p_signaled_packet_index) {
    std::lock_guard<std::mutex> thread_lock(m_signaled_packets_mutex);
    auto it = m_signaled_packets.find(p_signaled_packet_index);
    if(it != m_signaled_packets.end()){
        m_signaled_packets.erase(it);
    }
}

void Server::UDP_SendSignaledPackets() {

    m_signaled_packets_mutex.lock();
    is_signaled_packets_running = true;
    while (!m_signaled_packets.empty()) {
        m_signaled_packets_mutex.unlock();

        m_signaled_packets_mutex.lock();
        auto it = m_signaled_packets.begin();
        while (it != m_signaled_packets.end()) {
            if (it->second.should_send())
                it->second.get_session()->UDP_Send(it->second.get_packet());
            if (it->second.has_timed_out()) {
                auto last_it = it;
                ++it;
                m_signaled_packets.erase(last_it);
                continue;
            }
            ++it;
        }
        m_signaled_packets_mutex.unlock();
        m_signaled_packets_mutex.lock();
    }
    m_signaled_packets_mutex.unlock();
    is_signaled_packets_running = false;
}

void Server::TestActiveSessions() {
    while(m_is_running) {
        auto now = std::chrono::high_resolution_clock::now();
        auto last_ping_duration = std::chrono::duration_cast<std::chrono::microseconds>(
                now - m_last_ping_time).count();

        std::lock_guard<std::mutex> session_lock(m_sessions_mutex);

        auto it = m_sessions.begin();
        while (it != m_sessions.end()) {

            auto tcp_duration = std::chrono::duration_cast<std::chrono::microseconds>(
                    now - it->second->get_tcp_last_received()).count();
            auto udp_duration = std::chrono::duration_cast<std::chrono::microseconds>(
                    now - it->second->get_udp_last_received()).count();
            if (last_ping_duration > PING_RATE) {

                Packet session_test(m_observer->get_hook_index("eco-session-test"));
                session_test.AddArgument(false); // Is not a Response
                session_test.AddArgument(boost::uuids::to_string(it->second->get_session_uuid()));

                if(it->second->is_tcp_connected())
                    it->second->TCP_Send(session_test);

                // In this particular case, we never care to end the signal, the when a UDP packet arrives from an endpoint its
                // last_received time gets updated.
                if(it->second->is_udp_connected()) {
                    it->second->UDP_Send(session_test);
                }
                m_last_ping_time = std::chrono::high_resolution_clock::now();
            }

            if (tcp_duration > SESSION_TIMEOUT_DISCONNECT_RATE || udp_duration > SESSION_TIMEOUT_DISCONNECT_RATE) {

                auto udp_it = m_udp_session_keys.find(it->second->get_udp_endpoint());

                if (udp_it != m_udp_session_keys.end()) {
                    m_udp_session_keys.erase(udp_it);
                }
                auto uuid = it->second->get_session_uuid();
                auto current_it = it;
                ++it;
                m_sessions.erase(current_it);
                std::cout
                        << "[Network Server/Reg] Session, " << uuid << ", has been closed due to inactivity, The end user may have disconnected."
                        << std::endl;
                continue;
            }
            ++it;
        }
    }
}