#include <exception>
#include <boost/bind/bind.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "eco-network/exceptions/NetworkException.hpp"
#include "eco-network/config.h"
#include "eco-network/Session.hpp"

using namespace ecosystems::network;
using namespace ecosystems::network::exceptions;

boost::uuids::uuid SESSION_UNASSIGNED_UUID = boost::uuids::string_generator()("00000000-0000-0000-0000-000000000000");

Session::Session(boost::asio::io_context &p_io_context, std::shared_ptr<events::NetworkObserver>& p_observer, std::unique_ptr<boost::asio::ip::udp::socket>& p_udp_socket)
    : m_tcp_socket(p_io_context), m_session_uuid(SESSION_UNASSIGNED_UUID), m_observer(p_observer), m_udp_socket(p_udp_socket)
{
    m_tcp_last_received = std::chrono::high_resolution_clock::now();
    m_udp_last_received = std::chrono::high_resolution_clock::now();
}

void Session::TCP_BeginReceive() {
    m_tcp_socket.async_read_some(boost::asio::buffer(m_recv_buffer, MAX_PACKET_SEGMENT_SIZE),
                                 boost::bind(&Session::TCP_HandleReceive, this,
                                             boost::asio::placeholders::error,
                                             boost::asio::placeholders::bytes_transferred));
}

void Session::TCP_HandleReceive(const boost::system::error_code &p_error, size_t p_bytes_read) {
    if(!p_error) {
        update_tcp_last_receved();
        m_chunked_data.insert(m_chunked_data.end(), &m_recv_buffer[0], &m_recv_buffer[0] + p_bytes_read);

        while(m_chunked_data.size() >= sizeof(size_t)) {
            size_t packet_size = *reinterpret_cast<size_t *>(m_chunked_data.data());
            try {
                if(packet_size > MAX_COMPLETE_PACKET_SIZE) {
                    m_chunked_data = std::vector<char>();
                    throw NetworkException(NetworkExceptionCode::PacketTooBig, "PACKET TOO BIG! A packet header claimed the packet size would be " + std::to_string(packet_size) + "bytes. Which is larger than the MAX_COMPLETE_PACKET_SIZE of " + std::to_string(MAX_COMPLETE_PACKET_SIZE) + ".");
                }

                if(m_chunked_data.size() < packet_size) {
                    break;
                }

                Packet packet(m_chunked_data.data(), packet_size, TCP);

                m_observer->Trigger(packet.get_event_hook_index(), packet, shared_from_this());
                // Create a new vector consisting of all the information after p_bytes_read
                m_chunked_data = std::vector<char>(m_chunked_data.begin() + packet_size, m_chunked_data.end());
            } catch(NetworkException& e) {
                std::cerr << e.what() << std::endl;
                std::cerr << "This error is non-fatal." << std::endl;
                m_chunked_data = std::vector<char>(m_chunked_data.begin() + packet_size, m_chunked_data.end());
            }
        }

        TCP_BeginReceive();
    } else {
#ifdef NETWORK_VERBOSE
        std::cerr << "[Network - Session/TCP] Error receiving data: " << p_error.message() << std::endl;
#endif
    }
}

void Session::TCP_Send(Packet &p_packet) {
    p_packet.set_send_method(TCP);
    m_tcp_socket.async_send(boost::asio::buffer(p_packet.get_send_buffer(), p_packet.get_send_buffer_size()),
                            boost::bind(&Session::TCP_HandleSend, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
}

void Session::TCP_HandleSend(const boost::system::error_code &p_error, size_t p_bytes_sent) {
    if(p_error) {
#ifdef NETWORK_VERBOSE
        std::cout << "[Network - Session/TCP] Error Sending TCP Data: " << p_error.message() << std::endl;
#endif
    }
}

void Session::UDP_Send(Packet &p_packet) {
    p_packet.set_send_method(UDP);
    const char* send_buffer = p_packet.get_send_buffer();
    size_t buffer_size = p_packet.get_send_buffer_size();

    if(buffer_size > MAX_PACKET_SEGMENT_SIZE) {
        std::cerr << "UDP_Send tried to send a packet larger than the MAX_PACKET_SEGMENT_SIZE.\nsize: " << buffer_size << ", MAX_PACKET_SEGMENT_SIZE: " << MAX_PACKET_SEGMENT_SIZE << std::endl;
        std::cerr << "Packet has been dropped." << std::endl;
        return;
        //throw NetworkException(NetworkExceptionCode::PacketTooBig, "You attempted to send a UDP packet of size " + std::to_string(buffer_size) + "bytes, which is larger than the MAX_PACKET_SEGMENT_SIZE of " + std::to_string(MAX_PACKET_SEGMENT_SIZE) + "bytes.\n Your UDP packets should be small, packet chunking isn't handled by UDP. Either make your packets smaller, or change your Max packet segment size.");
    }

    m_udp_socket->async_send_to(boost::asio::buffer(send_buffer, buffer_size), m_udp_endpoint,
                                boost::bind(&Session::UDP_HandleSend, this, boost::asio::placeholders::error,
                                            boost::asio::placeholders::bytes_transferred));
}

void Session::UDP_HandleSend(const boost::system::error_code& p_error, std::size_t p_bytes_sent) {
    if(p_error) {
        std::cerr << "Error sending packet: " << p_error.message() << std::endl;
    }
}
