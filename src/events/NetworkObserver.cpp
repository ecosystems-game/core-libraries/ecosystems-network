#include "eco-network/events/NetworkObserver.hpp"
#include "eco-network/events/NetworkEvent.hpp"
#include "eco-network/Session.hpp"

using namespace ecosystems::network::events;

void NetworkObserver::Trigger(unsigned int p_hook_index, Packet& p_packet, std::shared_ptr<Session> p_session) {
    for(size_t i = 0; i < m_subscribers.size(); ++i)
    {
        if(m_subscribers[i] == nullptr)
            continue;

        if(m_subscribers[i]->get_hook_index() == p_hook_index)
        {
            try {
                m_subscribers[i]->Call(p_packet, p_session, shared_from_this());
            } catch(const std::exception& e) {
                std::cerr << "An error was thrown processing a network packet\n" << e.what() << std::endl;
            }
        }
    }
}

unsigned int NetworkObserver::get_hook_index(std::string p_hook) {
    auto it = m_index_map.find(p_hook);
    if(it != m_index_map.end())
    {
        return it->second;
    } else {
        return -1;
    }
}

std::string NetworkObserver::get_hook(unsigned int p_hook_index)
{
    auto it = m_hook_map.find(p_hook_index);
    if(it != m_hook_map.end())
    {
        return it->second;
    } else {
        return "undefined";
    }
}

unsigned int NetworkObserver::AppendHookToMap(std::string& p_hook) {
    auto it = m_index_map.find(p_hook);
    if(it == m_index_map.end())
    {
        m_index_map.insert(std::pair<std::string, int>(p_hook, m_hook_index));
        m_hook_map.insert(std::pair<int, std::string>(m_hook_index, p_hook));
        ++m_hook_index;

        return m_hook_index-1;
    } else {
        return it->second;
    }
}

void NetworkObserver::SubscribeEvent(std::string p_hook, NetworkEvent* p_network_event) {
    unsigned int p_hook_index = AppendHookToMap(p_hook);
    p_network_event->set_hook(p_hook, p_hook_index);
    m_subscribers.push_back(p_network_event);
}