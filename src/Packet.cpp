#include "eco-network/Packet.hpp"
#include "eco-network/exceptions/NetworkException.hpp"

using namespace ecosystems::network;
using namespace ecosystems::network::exceptions;

Packet::Packet(unsigned int p_event_hook_index)
        : m_event_hook_index(p_event_hook_index), m_send_method(LOCAL)
{
    // Adds an extra size_t spacer to fill out during get_send_buffer
    m_send_buffer.resize(sizeof(size_t) + sizeof(m_event_hook_index));
    m_write_cursor += sizeof(size_t);

    // Copy Event Hook
    std::memcpy(&m_send_buffer[m_write_cursor], reinterpret_cast<void*>(&m_event_hook_index), sizeof(m_event_hook_index));
    m_write_cursor += sizeof(m_event_hook_index);
}

Packet::Packet(const char* p_buffer, size_t p_bytes_read, PacketSendMethod p_send_method)
        : m_read_buffer(p_buffer), m_bytes_read(p_bytes_read), m_send_method(p_send_method)
{
    if(get_send_method() == TCP) {
        m_read_cursor += sizeof(size_t); // Because we don't need to actually copy packet size
    }

    // Protection from Buffer overloading
    if(m_read_cursor + sizeof(m_event_hook_index) > p_bytes_read) {
        std::cerr << "Attempted to read a packet, but it would have thrown a m_buffer overload exception" << std::endl;
        throw NetworkException(NetworkExceptionCode::BufferOverload, "Attempted to read a packet header, but it would've caused a m_buffer overload.");
    }

    // Copy Event Hook
    std::memcpy(&m_event_hook_index, p_buffer + m_read_cursor, sizeof(m_event_hook_index));
    m_read_cursor += sizeof(m_event_hook_index);
}


template<typename T> ECO_API T Packet::ReadNextArgument()
{
    if(m_read_cursor + sizeof(T) > m_bytes_read) {
        throw NetworkException(NetworkExceptionCode::BufferOverload, "Attempted to read an argument from a packet but it would've caused a m_buffer overload.");
    }

    T value = *reinterpret_cast<const T*>(&m_read_buffer[m_read_cursor]);
    m_read_cursor += sizeof(T);
    return value;
}

template<> ECO_API std::string Packet::ReadNextArgument<std::string>()
{
    if(m_read_cursor + sizeof(size_t) > m_bytes_read) {
        throw NetworkException(NetworkExceptionCode::BufferOverload, "Attempted to read an argument from a packet but it would've caused a m_buffer overload, it is possible that the argument does not exist in this packet!");
    }

    size_t str_size = *reinterpret_cast<const size_t*>(&m_read_buffer[m_read_cursor]);
    m_read_cursor += sizeof(size_t);

    if(m_read_cursor + str_size > m_bytes_read) {
        throw NetworkException(NetworkExceptionCode::BufferOverload, "Attempted to read a string from a packet but it would've caused a m_buffer overload, it is possible that the argument does not exist in this packet!");
    }

    const char* value = reinterpret_cast<const char*>(&m_read_buffer[m_read_cursor]);
    m_read_cursor += str_size;

    return std::string(value, str_size);
}

const char* Packet::get_send_buffer() {
    // Replace the packet_size place holder we added in the constructor

    if(get_send_method() == TCP) {
        size_t packet_size = m_send_buffer.size();
        std::memcpy(&m_send_buffer[0], reinterpret_cast<const char *>(&packet_size), sizeof(size_t));
        return &m_send_buffer[0];
    } else {
        return &m_send_buffer[sizeof(size_t)];
    }
}

template<typename T>
void Packet::AddArgument(T p_value) {
    m_send_buffer.resize(m_send_buffer.size() + sizeof(T));
    std::memcpy(&m_send_buffer[m_write_cursor], reinterpret_cast<const char*>(&p_value), sizeof(T));
    m_write_cursor += sizeof(T);
}

template<> ECO_API void Packet::AddArgument<std::string>(std::string p_value) {
    m_send_buffer.resize(m_send_buffer.size() + p_value.size() + sizeof(size_t));

    size_t data_size = p_value.size();
    std::memcpy(&m_send_buffer[m_write_cursor], reinterpret_cast<const char*>(&data_size), sizeof(data_size));
    m_write_cursor += sizeof(size_t);

    std::memcpy(&m_send_buffer[m_write_cursor], reinterpret_cast<const char*>(&p_value[0]), data_size);
    m_write_cursor += p_value.size();
}

template<> ECO_API void Packet::AddArgument<const char *>(const char * p_value) {
    AddArgument<std::string>(std::string(p_value)); // '\0'
}

template ECO_API int8_t Packet::ReadNextArgument<int8_t>();
template ECO_API int16_t Packet::ReadNextArgument<int16_t>();
template ECO_API int32_t Packet::ReadNextArgument<int32_t>();
template ECO_API int64_t Packet::ReadNextArgument<int64_t>();
template ECO_API long long Packet::ReadNextArgument<long long>();
template ECO_API uint8_t Packet::ReadNextArgument<uint8_t>();
template ECO_API uint16_t Packet::ReadNextArgument<uint16_t>();
template ECO_API uint32_t Packet::ReadNextArgument<uint32_t>();
template ECO_API uint64_t Packet::ReadNextArgument<uint64_t>();
template ECO_API unsigned long long Packet::ReadNextArgument<unsigned long long>();
template ECO_API float Packet::ReadNextArgument<float>();
template ECO_API double Packet::ReadNextArgument<double>();
template ECO_API bool Packet::ReadNextArgument<bool>();
template ECO_API char Packet::ReadNextArgument<char>();


template ECO_API void Packet::AddArgument<int8_t>(int8_t);
template ECO_API void Packet::AddArgument<int16_t>(int16_t);
template ECO_API void Packet::AddArgument<int32_t>(int32_t);
template ECO_API void Packet::AddArgument<int64_t>(int64_t);
template ECO_API void Packet::AddArgument<long long>(long long);
template ECO_API void Packet::AddArgument<uint8_t>(uint8_t);
template ECO_API void Packet::AddArgument<uint16_t>(uint16_t);
template ECO_API void Packet::AddArgument<uint32_t>(uint32_t);
template ECO_API void Packet::AddArgument<uint64_t>(uint64_t);
template ECO_API void Packet::AddArgument<unsigned long long>(unsigned long long);
template ECO_API void Packet::AddArgument<char>(char);
template ECO_API void Packet::AddArgument<float>(float);
template ECO_API void Packet::AddArgument<double>(double);
template ECO_API void Packet::AddArgument<bool>(bool);