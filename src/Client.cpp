#include "eco-network/Client.hpp"
#include "eco-network/exceptions/NetworkException.hpp"
#include "eco-network/eco-events/client/EcoSessionConnectEvent.hpp"
#include "eco-network/eco-events/client/EcoSessionTestConnection.hpp"

using namespace ecosystems::network;
using namespace ecosystems::network::exceptions;

Client::Client() : m_io_context(), m_observer(new events::NetworkObserver()),
    m_udp_socket(new boost::asio::ip::udp::socket(m_io_context)), m_signaled_packet_index(), m_session( new Session(m_io_context, m_observer, m_udp_socket))
{
    // Bind the default network events used by the internal library (eco_events)
    m_observer->SubscribeEvent("eco-session-connect", new events::client::eco_events::EcoSessionConnectEvent(this));
    m_observer->SubscribeEvent("eco-session-test", new events::client::eco_events::EcoSessionTestConnection(this));
}

void Client::Shutdown()  {
    m_session->get_tcp_socket().close();
    m_udp_socket->close();
    m_io_context.stop();
    m_client_thread.join();
}

void Client::Connect(const char* p_hostname, unsigned int p_port) {
    UDP_Open(p_hostname, p_port);
    TCP_Connect(p_hostname, p_port);
    m_client_thread = std::thread(&Client::client_thread, this);
}

void Client::client_thread()
{
    m_io_context.run();
};

void Client::UDP_Open(const char* p_hostname, unsigned int p_port) {
    auto port = std::to_string(p_port);

    m_server_ep = boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(p_hostname), p_port);
    m_udp_socket->open(boost::asio::ip::udp::v4());

    UDP_BeginReceive();
    std::cout << "[Network Client/UDP] UDP Socket has been open to the remote address, " << p_hostname << ":" << p_port << std::endl;
}

void Client::TCP_Connect(const char* p_hostname, unsigned int p_port) {
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(p_hostname), p_port);
    m_session->get_tcp_socket().connect(endpoint);
    m_session->set_tcp_connected(true);
    m_session->set_udp_endpoint(m_server_ep);
    std::cout << "[Network Client/TCP] TCP Socket has connected to the remote address, " << p_hostname << ":" << p_port << std::endl;
    m_session->TCP_BeginReceive();
}

void Client::UDP_BeginReceive() {
    m_udp_socket->async_receive_from(
            boost::asio::buffer(*m_recv_buffer), m_receiver_ep,
            boost::bind(&Client::UDP_HandleReceive, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
}

void Client::UDP_HandleReceive(const boost::system::error_code p_error, size_t p_bytes_read) {
    if(!p_error) {
        try {
            auto recv_buffer = *m_recv_buffer;
            Packet packet(&recv_buffer[0], p_bytes_read, UDP);
            m_observer->Trigger(packet.get_event_hook_index(), packet, m_session);
        } catch (NetworkException& e) {
            std::cerr << e.what() << std::endl;
        }
    } else if(p_bytes_read != 0) {
        std::cerr << "[Network/UDP] Error receiving data: " << p_error.message() << std::endl;
    }

    UDP_BeginReceive();
}

void Client::UDP_SignalStopSend(size_t p_signaled_packet_index) {
    std::lock_guard<std::mutex> thread_lock(m_signaled_packets_mutex);
    auto it = m_signaled_packets.find(p_signaled_packet_index);
    if(it != m_signaled_packets.end()){
        m_signaled_packets.erase(it);
    }
}

void Client::UDP_SendSignaledPackets() {

    m_signaled_packets_mutex.lock();
    is_signaled_packets_running = true;
    while(!m_signaled_packets.empty()) {
        m_signaled_packets_mutex.unlock();
        m_signaled_packets_mutex.lock();
        auto it = m_signaled_packets.begin();
        while(it != m_signaled_packets.end())
        {
            if(it->second.should_send())
                m_session->UDP_Send(it->second.get_packet());
            if(it->second.has_timed_out()) {
                auto last_it = it;
                ++it;
                m_signaled_packets.erase(last_it);
                continue;
            }
            ++it;
        }
        m_signaled_packets_mutex.unlock();
        m_signaled_packets_mutex.lock();
    }
    is_signaled_packets_running = false;
    m_signaled_packets_mutex.unlock();
}

size_t Client::UDP_SendUntilSignaled(Packet& p_packet, uint32_t p_relay_usec, uint32_t p_timeout_usec) {
    std::lock_guard<std::mutex> thread_lock(m_signaled_packets_mutex);

    size_t index = m_signaled_packet_index;
    m_signaled_packets.insert({index, SignaledPacket(p_packet, p_relay_usec, p_timeout_usec, m_session)});
    ++m_signaled_packet_index;

    if(!is_signaled_packets_running) {
        if (m_signaled_packets_thread.joinable()) {
            m_signaled_packets_thread.join();
            is_signaled_packets_running = true;
        }

        m_signaled_packets_thread = std::thread(&Client::UDP_SendSignaledPackets, this);
    }
    return index;
}