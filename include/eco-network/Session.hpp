#ifndef ECOSYSTEMS_SESSION_HPP
#define ECOSYSTEMS_SESSION_HPP

#include "EcoApiModule.h"
#include "config.h"
#include "Packet.hpp"
#include "eco-network/events/NetworkObserver.hpp"
#include <boost/asio.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

namespace ecosystems::network {

    class Session : public std::enable_shared_from_this<Session> {
    public:
        ECO_API Session(boost::asio::io_context& p_io_context, std::shared_ptr<events::NetworkObserver>& p_observer, std::unique_ptr<boost::asio::ip::udp::socket>& p_udp_socket);
        void TCP_BeginReceive();
        ECO_API void TCP_Send(Packet& p_packet);
        ECO_API void UDP_Send(Packet& p_packet);
        ECO_API boost::asio::ip::tcp::socket& get_tcp_socket() { return m_tcp_socket; }
        ECO_API boost::asio::ip::udp::endpoint& get_udp_endpoint() { return m_udp_endpoint; }
        ECO_API boost::uuids::uuid& get_session_uuid() { return m_session_uuid; }
        void set_session_uuid(boost::uuids::uuid p_uuid) { m_session_uuid = p_uuid; }

        void set_tcp_connected(bool p_value) { m_is_tcp_connected = p_value; }
        bool is_tcp_connected() { return m_is_tcp_connected; }
        void set_udp_connected(bool p_value) { m_is_udp_connected = p_value; }
        bool is_udp_connected() { return m_is_udp_connected; }
        ECO_API bool is_connected() { return m_is_udp_connected && m_is_tcp_connected; }

        void set_udp_endpoint(boost::asio::ip::udp::endpoint& p_udp_endpoint) { m_udp_endpoint = p_udp_endpoint; }
        ECO_API Packet CreatePacket(std::string p_hook) { return Packet(m_observer->get_hook_index(std::move(p_hook))); }
        void update_udp_last_received() { m_udp_last_received = std::chrono::high_resolution_clock::now(); }
        void update_tcp_last_receved() { m_tcp_last_received = std::chrono::high_resolution_clock::now(); }
        ECO_API std::chrono::time_point<std::chrono::high_resolution_clock>& get_udp_last_received() { return m_udp_last_received; }
        ECO_API std::chrono::time_point<std::chrono::high_resolution_clock>& get_tcp_last_received() { return m_tcp_last_received; }
    private:
        void UDP_HandleSend(const boost::system::error_code& p_error, std::size_t p_bytes_sent);
        void TCP_HandleReceive(const boost::system::error_code &p_error, size_t p_bytes_read);
        void TCP_HandleSend(const boost::system::error_code &p_error, size_t p_bytes_sent);

        bool m_is_tcp_connected = false;
        bool m_is_udp_connected = false;
        boost::asio::ip::udp::endpoint m_udp_endpoint;
        boost::asio::ip::tcp::socket m_tcp_socket;
        boost::uuids::uuid m_session_uuid;
        std::vector<char> m_chunked_data;
        char m_recv_buffer[MAX_PACKET_SEGMENT_SIZE];
        std::shared_ptr<events::NetworkObserver>& m_observer;
        std::unique_ptr<boost::asio::ip::udp::socket>& m_udp_socket;
        std::chrono::time_point<std::chrono::high_resolution_clock> m_tcp_last_received, m_udp_last_received;
    };
}
#endif //ECOSYSTEMS_SESSION_HPP
