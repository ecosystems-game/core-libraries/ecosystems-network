#ifndef ECOSYSTEMS_INCOMINGCHATEVENT_HPP
#define ECOSYSTEMS_INCOMINGCHATEVENT_HPP

#include <thread>
#include <chrono>
#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <string>
#include <boost/lexical_cast.hpp>
#include "eco-network/events/NetworkEvent.hpp"
#include "eco-network/exceptions/NetworkException.hpp"
#include "eco-network/Packet.hpp"
#include "eco-network/Session.hpp"
#include "eco-network/Client.hpp"

namespace ecosystems::network::events::client::eco_events {
    class EcoSessionConnectEvent : public network::events::NetworkEvent {
    public:
        EcoSessionConnectEvent(Client* p_client) : network::events::NetworkEvent(), m_client(p_client) {}
        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<events::NetworkObserver> p_observer) override {
            bool is_response = p_packet.ReadNextArgument<bool>();

            // Server responds with a UDP packet to let us know the session has been connected
            if(is_response && p_packet.get_send_method() == UDP) {
                std::cout << "[Network/UDP] Received response from session registration" << std::endl;
                m_client->UDP_SignalStopSend(m_sig_packet_id);
                p_session->set_udp_connected(true);
                return;
            } else if(is_response && p_packet.get_send_method() != UDP) {
                std::cerr << "[Network/UDP] Response was true but packet was received on a TCP line, this should not happen." << std::endl;
                return;
            }

            auto uuid_str = p_packet.ReadNextArgument<std::string>();
            auto uuid = boost::lexical_cast<boost::uuids::uuid>(uuid_str);
            p_session->set_session_uuid(uuid);

            // If this packet came in, it means that TCP is connected to the session
            p_session->set_tcp_connected(true);
            std::cout << "[Network - Client/TCP] Received Eco-Session Connection Updated Session UUID: " << uuid << std::endl;

            Packet udp_connect(p_observer->get_hook_index("eco-session-connect"));
            udp_connect.AddArgument(uuid_str);
            m_sig_packet_id = m_client->UDP_SendUntilSignaled(udp_connect, 1000000, 15000000); // Send every second, timeout after 15
        }
    private:
        Client* m_client;
        size_t m_sig_packet_id = 0;
    };
}

#endif //ECOSYSTEMS_INCOMINGCHATEVENT_HPP
