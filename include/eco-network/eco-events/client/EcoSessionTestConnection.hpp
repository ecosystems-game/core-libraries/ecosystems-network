#ifndef ECOSYSTEMS_ECOSESSIONTESTCONNECTION_HPP
#define ECOSYSTEMS_ECOSESSIONTESTCONNECTION_HPP

#include <thread>
#include <chrono>
#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <string>
#include <boost/lexical_cast.hpp>
#include "eco-network/events/NetworkEvent.hpp"
#include "eco-network/exceptions/NetworkException.hpp"
#include "eco-network/Packet.hpp"
#include "eco-network/Session.hpp"
#include "eco-network/Client.hpp"

// Event to validate that a session is fully connected on UDP and TCP
namespace ecosystems::network::events::client::eco_events {
    class EcoSessionTestConnection : public network::events::NetworkEvent {
    public:
        EcoSessionTestConnection(Client* p_client) : network::events::NetworkEvent(), m_client(p_client) {}
        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<events::NetworkObserver> p_observer) override {
            // All we're doing is sending the SessionID back to the server, to confirm the connection is valid
            Packet packet(p_observer->get_hook_index("eco-session-test"));
            packet.AddArgument(to_string(p_session->get_session_uuid()));
            if(p_packet.get_send_method() == TCP) {
                p_session->TCP_Send(packet);
            } else if(p_packet.get_send_method() == UDP) {
                p_session->UDP_Send(packet);
            }
        }

    private:

        Client* m_client;
    };
}

#endif //ECOSYSTEMS_ECOSESSIONTESTCONNECTION_HPP
