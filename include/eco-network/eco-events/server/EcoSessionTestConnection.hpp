#ifndef ECOSYSTEMS_ECOSESSIONTESTCONNECTION_HPP
#define ECOSYSTEMS_ECOSESSIONTESTCONNECTION_HPP

#include <thread>
#include <chrono>
#include <string>

#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

#include "eco-network/events/NetworkEvent.hpp"
#include "eco-network/Packet.hpp"
#include "eco-network/SignaledPacket.hpp"
#include "eco-network/Session.hpp"
#include "eco-network/Client.hpp"


// Event to validate that a session is fully connected on UDP and TCP
namespace ecosystems::network::events::server::eco_events {
    class EcoSessionTestConnection : public network::events::NetworkEvent {
    public:
        EcoSessionTestConnection(Server* p_server) : network::events::NetworkEvent(), m_server(p_server) {}
        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<events::NetworkObserver> p_observer) override {
            if(p_packet.get_send_method() == TCP) {
                p_session->set_tcp_connected(true);
            } else if(p_packet.get_send_method() == UDP) {
                p_session->set_udp_connected(true);
            }
        }

    private:
        Server* m_server;
    };
}

#endif //ECOSYSTEMS_ECOSESSIONTESTCONNECTION_HPP
