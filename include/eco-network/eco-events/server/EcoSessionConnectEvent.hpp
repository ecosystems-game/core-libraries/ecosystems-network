#ifndef ECOSYSTEMS_INCOMINGCHATEVENT_HPP
#define ECOSYSTEMS_INCOMINGCHATEVENT_HPP

#include <boost/uuid/uuid_io.hpp>
#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/throw_exception.hpp>
#include "eco-network/events/NetworkEvent.hpp"
#include "eco-network/Packet.hpp"
#include "eco-network/Session.hpp"
#include "eco-network/Server.hpp"

namespace ecosystems::network::events::eco_events::server {
    class EcoSessionConnectEvent : public network::events::NetworkEvent {
    public:
        EcoSessionConnectEvent(Server* p_server) : network::events::NetworkEvent(), m_server(p_server) {}
        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<events::NetworkObserver> p_observer) override {
            if(p_packet.get_send_method() == TCP)
                return;

            auto uuid_str = p_packet.ReadNextArgument<std::string>();
            boost::uuids::uuid uuid = boost::uuids::nil_uuid();
            try {
                uuid = boost::lexical_cast<boost::uuids::uuid>(uuid_str);
            } catch(boost::bad_lexical_cast const& e) {
                std::cerr << "Failed to cast EcoSession UUID" << std::endl;
                return;
            }
            Packet connect_response(p_observer->get_hook_index("eco-session-connect"));
            connect_response.AddArgument(true); // Set Response to true

            if(!p_session->is_tcp_connected())
                m_server->UDP_BindEndpointToSession(p_session->get_udp_endpoint(), uuid);

            if(m_server->get_session(uuid) == nullptr) return;
            m_server->get_session(uuid)->UDP_Send(connect_response);
        }

    private:
        Server* m_server;
    };
}

#endif //ECOSYSTEMS_INCOMINGCHATEVENT_HPP
