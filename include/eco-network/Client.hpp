#ifndef ECOSYSTEMS_CLIENT_HPP
#define ECOSYSTEMS_CLIENT_HPP

#include <memory>
#include <thread>
#include <mutex>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>

#include "EcoApiModule.h"
#include "eco-network/Session.hpp"
#include "eco-network/SignaledPacket.hpp"
#include "eco-network/exceptions/NetworkException.hpp"
#include "eco-network/events/NetworkObserver.hpp"
#include "eco-network/events/NetworkEvent.hpp"

namespace ecosystems::network {

    class Client {
    public:
        ECO_API Client();
        ECO_API ~Client() { Shutdown(); }
        ECO_API void Connect(const char* p_hostname, unsigned int p_port);
        ECO_API void Shutdown();
        ECO_API void TCP_Send(Packet& p_packet){ m_session->TCP_Send(p_packet); }
        ECO_API void UDP_Send(Packet& p_packet){ m_session->UDP_Send(p_packet); }

        // Well send data on a loop in an async thread until you signal it to stop
        // Send Rate in microseconds
        // Returns the ID of the signaled packet
        ECO_API size_t UDP_SendUntilSignaled(Packet& p_packet, uint32_t p_relay_usec, uint32_t p_timeout_usec);
        ECO_API void UDP_SignalStopSend(size_t p_signaled_packet_index);
        ECO_API std::shared_ptr<ecosystems::network::events::NetworkObserver>& get_observer() { return m_observer; }
        ECO_API std::shared_ptr<ecosystems::network::Session>& get_session() { return m_session; }
    private:
        void UDP_SendSignaledPackets();
        void TCP_Connect(const char* p_hostname, unsigned int p_port);
        void UDP_Open(const char* p_hostname, unsigned int p_port);
        void UDP_BeginReceive();
        void UDP_HandleReceive(const boost::system::error_code p_error, size_t p_bytes_read);
        void client_thread();

        boost::asio::io_context m_io_context;
        size_t m_signaled_packet_index = 0;
        bool is_signaled_packets_running = false;
        std::mutex m_signaled_packets_mutex;
        std::thread m_signaled_packets_thread, m_client_thread;
        std::map<size_t, SignaledPacket> m_signaled_packets;
        std::unique_ptr<boost::asio::ip::udp::socket> m_udp_socket;
        boost::asio::ip::udp::endpoint m_server_ep, m_receiver_ep; // m_server_ep is used for the initial connection, m_receiver_ep is through UDP_BeginReceive and UDP_HandleReceive
        std::unique_ptr<boost::array<char, 1024>> m_recv_buffer = std::make_unique<boost::array<char, 1024>>();
        std::shared_ptr<ecosystems::network::events::NetworkObserver> m_observer;
        std::shared_ptr<ecosystems::network::Session> m_session;
    };
}

#endif //ECOSYSTEMS_CLIENT_HPP
