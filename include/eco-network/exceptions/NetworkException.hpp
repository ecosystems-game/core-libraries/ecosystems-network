#ifndef ECOSYSTEMS_NETWORKEXCEPTION_HPP
#define ECOSYSTEMS_NETWORKEXCEPTION_HPP

#include <stdexcept>
#include <string>

namespace ecosystems::network::exceptions {

    enum class NetworkExceptionCode {
        Undefined = 0,
        BufferOverload,
        PacketTooBig,
        TCPSendFailed,
        UDPSignaledPacketTimedOut
    };

    class NetworkException : public std::runtime_error {
    public:
        NetworkException(NetworkExceptionCode p_code, std::string msg)
            : std::runtime_error(msg), m_network_exception_code(p_code)
        {}

        NetworkExceptionCode get_network_exception_code() { return m_network_exception_code; };
    private:
        NetworkExceptionCode m_network_exception_code;
    };
}

#endif //ECOSYSTEMS_NETWORKEXCEPTION_HPP
