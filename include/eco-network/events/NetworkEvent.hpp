#ifndef ECOSYSTEMS_NETWORKEVENT_HPP
#define ECOSYSTEMS_NETWORKEVENT_HPP

#include "EcoApiModule.h"
#include <string>
#include <boost/asio.hpp>
#include "eco-network/Packet.hpp"
#include "eco-network/exceptions/NetworkException.hpp"

namespace ecosystems::network {
    class Session;
}

namespace ecosystems::network::events {
    class NetworkObserver;

    class NetworkEvent{
    public:
        ECO_API void set_hook(std::string p_hook, unsigned int p_hook_index) {
            m_hook = p_hook;
            m_hook_index = p_hook_index;
        }
        ECO_API std::string& get_hook() { return m_hook; }
        ECO_API unsigned int get_hook_index() { return m_hook_index; }
        ECO_API virtual void Call(ecosystems::network::Packet&, std::shared_ptr<ecosystems::network::Session>&, std::shared_ptr<events::NetworkObserver> p_observer) {}
        ECO_API virtual void OnNetworkError(exceptions::NetworkException& e, std::shared_ptr<network::Session>& p_session) {}
    private:
        std::string m_hook = "undefined";
        unsigned int m_hook_index = -1;
    };
}

#endif //ECOSYSTEMS_NETWORKEVENT_HPP
