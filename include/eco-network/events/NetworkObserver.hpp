#ifndef ECOSYSTEMS_NETWORKOBSERVER_HPP
#define ECOSYSTEMS_NETWORKOBSERVER_HPP

#include "EcoApiModule.h"
#include <map>
#include <memory>
#include <vector>
#include <string>
#include <boost/asio.hpp>
#include "eco-network/Packet.hpp"
#include "NetworkEvent.hpp"

namespace ecosystems::network {
    class Session;
}

namespace ecosystems::network::events {
    class NetworkObserver : public std::enable_shared_from_this<NetworkObserver> {
    public:
        ECO_API NetworkObserver() : m_index_map(), m_hook_map(), m_subscribers() {}
        ECO_API void Trigger(unsigned int p_hook_index, Packet& packet, std::shared_ptr<Session> p_session);
        ECO_API void SubscribeEvent(std::string p_hook, NetworkEvent* p_network_event);

        ECO_API bool is_hook_index_in_map(uint32_t p_hook) {
            auto it = m_hook_map.find(p_hook);
            if(it != m_hook_map.end())
                return true;
            return false;
        }
        ECO_API unsigned int get_hook_index(std::string p_hook);
        ECO_API std::string get_hook(unsigned int p_hook_index);
    private:
        unsigned int AppendHookToMap(std::string& p_hook);

        unsigned int m_hook_index = 0;

        // Used to find hook indexes by hook name
        std::map<std::string, unsigned int> m_index_map;

        // Used to find Hook Names by Indexes;
        std::map<unsigned int, std::string> m_hook_map;

        // TODO: Possibly convert to smart pointers
        std::vector<NetworkEvent*> m_subscribers;
    };
}

#endif //ECOSYSTEMS_NETWORKOBSERVER_HPP
