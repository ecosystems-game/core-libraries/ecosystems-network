#ifndef ECOSYSTEMS_SIGNALEDPACKET_HPP
#define ECOSYSTEMS_SIGNALEDPACKET_HPP

#include "EcoApiModule.h"
#include <chrono>
#include "Packet.hpp"
#include "eco-network/exceptions/NetworkException.hpp"

namespace ecosystems::network {

    class SignaledPacket {
    public:
        SignaledPacket(Packet& p_packet, uint32_t p_relay, uint32_t p_timeout, std::shared_ptr<Session>& p_session)
            : m_packet(p_packet), m_relay(p_relay), m_timeout(p_timeout), m_session(p_session)
        {
            m_signal_time = std::chrono::high_resolution_clock::now();
            m_last_send_time = std::chrono::high_resolution_clock::now();
        }


        ECO_API bool should_send() {
            auto current_time = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(current_time - m_last_send_time).count();

            if(duration > m_relay) {
                m_last_send_time = std::chrono::high_resolution_clock::now();
                return true;
            }

            return false;
        }

        ECO_API bool has_timed_out() {
            auto current_time = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(current_time - m_signal_time).count();

            if(duration > m_timeout && m_timeout > 0) {
                return true;
            }

            return false;
        }

        ECO_API Packet& get_packet() { return m_packet; }
        ECO_API std::shared_ptr<Session>& get_session() { return m_session; };
    private:
        uint32_t m_relay, m_timeout;
        Packet m_packet;
        std::chrono::time_point<std::chrono::high_resolution_clock> m_signal_time = std::chrono::high_resolution_clock::now();
        std::chrono::time_point<std::chrono::high_resolution_clock> m_last_send_time = std::chrono::high_resolution_clock::now();
        std::shared_ptr<Session> m_session;
    };
}

#endif //ECOSYSTEMS_SIGNALEDPACKET_HPP
