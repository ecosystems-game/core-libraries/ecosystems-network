#ifndef ECOSYSTEMS_PACKET_HPP
#define ECOSYSTEMS_PACKET_HPP

#include "EcoApiModule.h"
#include <string>
#include <memory>
#include <vector>
#include <iostream>
#include <cstdint>
#include <cstring>

namespace ecosystems::network {
    enum PacketSendMethod { LOCAL, UDP, TCP };

    class Packet {
    public:
        ECO_API Packet(unsigned int p_event_hook_index);
        ECO_API Packet(const char* p_buffer, size_t bytes_read, PacketSendMethod p_send_method);

        ECO_API void AddArgument(const void* p_value, size_t p_argument_size)
        {
            m_send_buffer.resize(m_send_buffer.size() + p_argument_size);
            std::memcpy(&m_send_buffer[m_write_cursor], reinterpret_cast<const char*>(p_value), p_argument_size);
            m_write_cursor += p_argument_size;
        }

        ECO_API const char* get_send_buffer();
        ECO_API unsigned int get_event_hook_index() { return m_event_hook_index; }
        template<typename T> ECO_API T ReadNextArgument();
        template<typename T> ECO_API void AddArgument(T p_value);

        size_t get_send_buffer_size() {
            if(get_send_method() == UDP)
                return m_send_buffer.size() - sizeof(size_t);
            else
                return m_send_buffer.size();
        }

        PacketSendMethod get_send_method() { return m_send_method; }
        void set_send_method(PacketSendMethod p_send_method) { m_send_method = p_send_method; }

        // Copies the read m_buffer to the write m_buffer
        ECO_API void EchoReadBuffer() {
            m_send_buffer.resize(m_bytes_read);
            std::memcpy(&m_send_buffer[0], m_read_buffer, m_bytes_read);
        }

    private:
        PacketSendMethod m_send_method;
        const char* m_read_buffer;
        std::vector<char> m_send_buffer;
        size_t m_bytes_read = 0;
        size_t m_read_cursor = 0;
        size_t m_write_cursor = 0;
        unsigned int m_event_hook_index = 0;
    };
}
#endif //ECOSYSTEMS_PACKET_HPP
