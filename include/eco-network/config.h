#ifndef ECOSYSTEMS_CONFIG_H
#define ECOSYSTEMS_CONFIG_H

#include <cstdint>


enum {
    // The max size of data (in bytes) a socket's Receive Buffer will be, limiting packet segment sizes
    // UDP Packets larger than this will be automatically chunked by the sockets. UDP Packets should be small enough
    // to fire once and forget, if you need to ensure the order of your data and that it all arrives, you should use
    // TCP instead. If you actually do need larger UDP packets, increase this value, but note it will also increase
    // the m_buffer size on the TCP Sockets as well.
    MAX_PACKET_SEGMENT_SIZE = 1024,

    // The max size a completed network packet can be. Scale this variable as needed, but don't give it a higher value then
    // Necessary. This value is in here for security. It ensures that someone doesn't send a packet with a promised m_buffer size
    // that will kill the server. Any packet received with a size larger than this will be dropped, and an error will be posted to the console.
    MAX_COMPLETE_PACKET_SIZE = 4096,
};

// The time period in microseconds in which we will send pings to the sessions to determine if they're still connected
static const uint32_t PING_RATE = 2500000; // Default is 2.5 seconds

// If we haven't received both TCP and UDP data (send by pings) within this many microseconds, we will forcibly close the session, and remove it from the server
static const uint32_t SESSION_TIMEOUT_DISCONNECT_RATE = 15000000; // Default is 15 seconds

#endif //ECOSYSTEMS_CONFIG_H
