#ifndef AI_SANDBOX_SERVER_HPP
#define AI_SANDBOX_SERVER_HPP

#include "EcoApiModule.h"
#include <memory>
#include <vector>
#include <map>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "Session.hpp"
#include "eco-network/events/NetworkObserver.hpp"
#include "config.h"
#include "SignaledPacket.hpp"

namespace ecosystems::network {
    class Server {
    public:
        ECO_API Server(unsigned short p_port);
        ECO_API void Run();
        ECO_API void Shutdown();

        ECO_API std::shared_ptr<Session> get_session(boost::uuids::uuid p_uuid);
        std::map<boost::uuids::uuid, std::shared_ptr<Session>> get_sessions() { 
            std::unique_lock<std::mutex>(m_sessions_mutex);
            return m_sessions;
        };
        void TestActiveSessions();
        ECO_API std::shared_ptr<events::NetworkObserver>& get_observer() { return m_observer; }
        ECO_API void UDP_BindEndpointToSession(boost::asio::ip::udp::endpoint& p_endpoint, boost::uuids::uuid p_uuid);
        ECO_API size_t UDP_SendUntilSignaled(Packet& p_packet, uint32_t p_relay_usec, uint32_t p_timeout_usec, std::shared_ptr<Session>& p_session);
        ECO_API void UDP_SignalStopSend(size_t p_signaled_packet_index);
    private:
        void RunThread();
        void UDP_SendSignaledPackets();
        void TCP_BeginAccept();
        void UDP_BeginReceive();
        void TCP_HandleAccept(std::shared_ptr<Session> p_session, const boost::system::error_code& p_error);
        void UDP_HandleReceive(const boost::system::error_code p_error, size_t p_bytes_transferred);

        unsigned int m_port;
        std::thread m_server_run_thread;
        boost::asio::io_context m_io_context;
        std::shared_ptr<events::NetworkObserver> m_observer;
        std::unique_ptr<boost::asio::ip::udp::socket> m_udp_socket;
        boost::asio::ip::tcp::acceptor m_tcp_acceptor;
        boost::asio::ip::udp::endpoint m_remote_ep;
        std::unique_ptr<boost::array<char, MAX_PACKET_SEGMENT_SIZE>> m_recv_buffer = std::make_unique<boost::array<char, MAX_PACKET_SEGMENT_SIZE>>();
        std::mutex m_sessions_mutex; // Used to protect both m_udp_session_keys and m_sessions
        std::map<boost::asio::ip::udp::endpoint, boost::uuids::uuid> m_udp_session_keys;
        std::map<boost::uuids::uuid, std::shared_ptr<Session>> m_sessions;
        bool m_is_running = false;
        bool is_signaled_packets_running = false;
        size_t m_signaled_packet_index = 1;
        std::mutex m_signaled_packets_mutex;
        std::thread m_signaled_packets_thread;
        std::thread m_ping_thread;
        std::map<size_t, SignaledPacket> m_signaled_packets;
        std::chrono::time_point<std::chrono::high_resolution_clock> m_last_ping_time;
    };
}

#endif //AI_SANDBOX_SERVER_HPP
